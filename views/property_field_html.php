<?php
\Bitrix\Main\UI\Extension::load("ui.vue");
/**
 * @var BxModernDirectory self
 * @var string $js_event
 * @var array $mask_labels
 * @var array $arr_property
 * @var array $control
 */
?>
<div id="moderndirectory-root-<?= $arr_property['ID'] ?>" class="moderndirectory">
    <div v-if="row.ID" class="moderndirectory__row">
        <input type="hidden" :value="row.ID" name="<?= $control['VALUE'] ?>">
        {{row_title}} &nbsp; <a href="javascript:void(0)" @click="edit_row"><?= GetMessage('BX_MODERNDIRECTORY_ROW_EDIT_TITLE'); ?></a>&nbsp;|&nbsp;<a @click="delete_row" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_DELETE_TITLE'); ?></a>
    </div>
    <div v-if="!row.ID" class="moderndirectory__add-link">
        <a @click="add_row" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_ADD_TITLE') ?></a> <span v-if="rows_count>=1">| <a @click="get_list" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_CHOOSE_TITLE') ?></a></span>
    </div>
</div>


<? ob_start(); ?>
<style>
    .moderndirectory {
        margin-bottom: 15px;
    }
    .moderndirectory__row {
        font-size: 14px;
    }
    .moderndirectory__row_line-through {
        text-decoration: line-through;
    }
</style>
<?
Bitrix\Main\Page\Asset::getInstance()->addString(ob_get_clean());
ob_start();
?>
<script>
    document.addEventListener("DOMContentLoaded", () => {

        BX.Vue.create({
            el: "#moderndirectory-root-<?= $arr_property['ID'] ?>",
            created: function () {
                BX.Vue.event.$on('<?= $js_event ?>', (data) => {
                    this.row = data.row;
                    this.rows_count = data.rows_count;
                });
            },
            data: {
                row: <?= json_encode($row) ?>,
                rows_count: <?= self::_directoryRowsCount($arr_property) ?>
            },
            methods: {
                add_row: function () {
                    window.jsUtils.OpenWindow("/bitrix/admin/bxmoderndirectory_add_edit_row.php?js_event=<?= $js_event ?>&directory_id=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>", 800, 700);
                },
                edit_row: function () {
                    window.jsUtils.OpenWindow(`/bitrix/admin/bxmoderndirectory_add_edit_row.php?js_event=<?= $js_event ?>&directory_id=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>&row_id=${this.row.ID}`, 800, 700);
                },
                delete_row: function () {
                    this.row = {};
                },
                get_list: function () {
                    window.jsUtils.OpenWindow(`/bitrix/admin/bxmoderndirectory_get_list.php?js_event=<?= $js_event ?>&ENTITY_ID=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>`, 800, 800);
                }
            },
            computed: {
                row_title: function () {
                    <? if (!empty($mask_labels)) { ?>
                        let result = '<?= $arr_property['USER_TYPE_SETTINGS']['MASK_VALUE'] ?>';
                        <? foreach ($mask_labels as $label): ?>
                            result = result.split("#<?= $label ?>#").join(this.row.<?= $label ?>);
                            <? endforeach ?>
                        return `#${this.row.ID}: ${result}`;
                    <? } else { ?>
                        if (this.row.UF_NAME_<?= LANGUAGE_ID?>) {
                            return `#${this.row.ID}: ${this.row.UF_NAME_<?= LANGUAGE_ID?>}`;
                        } else if (this.row.UF_NAME) {
                            return `#${this.row.ID}: ${this.row.UF_NAME}`;
                        } else {
                            return `<?= GetMessage('BX_MODERNDIRECTORY_ROW_TITLE') ?> #${this.row.ID}`;
                        }
                        
                    <? } ?>
                }
            }
        });
    });
</script>
<?
Bitrix\Main\Page\Asset::getInstance()->addString(ob_get_clean());
