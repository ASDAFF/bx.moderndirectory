<?php
\Bitrix\Main\UI\Extension::load("ui.vue");
/**
 * @var BxModernDirectory self
 * @var array $mask_labels
 * @var array $arr_property
 * @var array $control
 */
?>
<div id="moderndirectory-root-<?= $arr_property['ID'] ?>" class="moderndirectory">
    <div v-for="row in rows" class="moderndirectory__row">
        <input v-if='!row.is_deleted' type="hidden" :value="row.ID" :name="row.control_name">
        <span v-if='!row.is_deleted'>{{row_title(row)}}</span>
        <span class="moderndirectory__row_line-through" v-if='row.is_deleted'>{{row_title(row)}}</span>
        <span v-if='!row.is_deleted'>&nbsp; <a href="javascript:void(0)" :data-row-id="row.ID" @click="edit_row"><?= GetMessage('BX_MODERNDIRECTORY_ROW_EDIT_TITLE'); ?></a>&nbsp;|&nbsp;</span><span v-if="!row.is_deleted"><a :data-row-id="row.ID" @click="delete_row" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_DELETE_TITLE'); ?></a></span>
        <span v-if='row.is_deleted'>&nbsp; <a href="javascript:void(0)" :data-row-id="row.ID" @click="return_row"><?= GetMessage('BX_MODERNDIRECTORY_ROW_RETURN_TITLE'); ?></a></span>
    </div>
    <div class="moderndirectory__add-link">
        <a @click="add_row" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_ADD_TITLE') ?></a> <span v-if="rows_count>=1">| <a @click="get_list" href="javascript:void(0)"><?= GetMessage('BX_MODERNDIRECTORY_ROW_CHOOSE_TITLE') ?></a></span>
    </div>
</div>


<? ob_start(); ?>
<style>
    .moderndirectory__row {
        font-size: 14px;
        margin-bottom: 10px;
    }
    .moderndirectory__row_line-through {
        text-decoration: line-through;
    }
</style>
<?
Bitrix\Main\Page\Asset::getInstance()->addString(ob_get_clean());
ob_start();
?>
<script>
    document.addEventListener("DOMContentLoaded", () => {

        BX.Vue.create({
            el: "#moderndirectory-root-<?= $arr_property['ID'] ?>",
            created: function () {
                BX.Vue.event.$on('<?= $js_event?>', (data) => {
                    if (!Array.isArray(this.rows) && this.rows && typeof this.rows === 'object') {
                        
                        let rows = this.rows;
                        this.rows = {};
                        
                        if (rows[data.row.ID]) {
                            for (let k in data.row) {
                                 
                                if (data.row.hasOwnProperty(k) && rows[data.row.ID].hasOwnProperty(k)) {
                                    rows[data.row.ID][k] = data.row[k];
                                }
                            }
                        } else {
                            rows[data.row.ID] = data.row;
                            rows[data.row.ID].control_name = '<?= $control['VALUE']?>[]';
                        }
                        rows[data.row.ID].is_deleted = false;
                        this.rows = rows;
                    } else {
                        this.rows = {};
                        this.rows[data.row.ID] = data.row;
                        this.rows[data.row.ID].is_deleted = false;
                        this.rows[data.row.ID].control_name = '<?= $control['VALUE']?>[]';
                    }
                    this.rows_count = data.rows_count;
                });
            },
            data: {
                rows: <?= json_encode($rows) ?>,
                rows_count: <?= self::_directoryRowsCount($arr_property) ?>
            },
            methods: {
                add_row: function () {
                    window.jsUtils.OpenWindow("/bitrix/admin/bxmoderndirectory_add_edit_row.php?js_event=<?= $js_event ?>&directory_id=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>", 800, 700);
                },
                edit_row: function (e) {
                    let row_id = e.target.dataset.rowId;
                    window.jsUtils.OpenWindow(`/bitrix/admin/bxmoderndirectory_add_edit_row.php?js_event=<?= $js_event ?>&directory_id=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>&row_id=${row_id}`, 800, 700);
                },
                delete_row: function (e) {
                    let row_id = e.target.dataset.rowId;
                    let rows = this.rows;
                    this.rows = {};
                    rows[row_id].is_deleted = true;
                    this.rows = rows;
                },
                return_row: function (e) {
                    let row_id = e.target.dataset.rowId;
                    let rows = this.rows;
                    this.rows = {};
                    rows[row_id].is_deleted = false;
                    this.rows = rows;
                },
                get_list: function () {
                    window.jsUtils.OpenWindow(`/bitrix/admin/bxmoderndirectory_get_list.php?js_event=<?= $js_event ?>&ENTITY_ID=<?= self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) ?>&lang=<?= LANGUAGE_ID ?>`, 800, 800);
                },                        
                row_title: function (row) {
                    
                    <? if (!empty($mask_labels)) { ?>
                        let result = '<?= $arr_property['USER_TYPE_SETTINGS']['MASK_VALUE'] ?>';
                        <? foreach ($mask_labels as $label): ?>
                            result = result.split("#<?= $label ?>#").join(row.<?= $label ?>);
                            <? endforeach ?>
                        return `#${row.ID}: ${result}`;
                    <? } else { ?>
                        if (row.UF_NAME_<?= LANGUAGE_ID?>) {
                            return `#${row.ID}: ${row.UF_NAME_<?= LANGUAGE_ID?>}`;
                        } else if (row.UF_NAME) {
                            return `#${row.ID}: ${row.UF_NAME}`;
                        } else {
                            return `<?= GetMessage('BX_MODERNDIRECTORY_ROW_TITLE') ?> #${row.ID}`;
                        }
                    <? } ?>
                }
            }
        });
    });
</script>
<?
Bitrix\Main\Page\Asset::getInstance()->addString(ob_get_clean());
