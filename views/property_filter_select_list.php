<?php
/**
 * @var BxModernDirectory self
 * @var array $arr_property
 * @var array $control
 * @var array $values
 * @var boolean $is_multiple
 */
?>

<select name="<?= $control['VALUE']?>">
    <option value=""><?= GetMessage('BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE') ?>
        <? foreach ($rows as $row): ?>
        <option <?if(in_array($row['ID'], $values)):?>selected<?endif?> value="<?= $row['ID'] ?>"><?= $row['DISPLAY'] ?></option>
    <? endforeach ?>
</select>
