<?php
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @global CUser $USER */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Highloadblock as HL;

Loc::loadMessages(__FILE__);

Bitrix\Main\Loader::includeModule('highloadblock');

// get entity settings
$hblockName = '';
$hlblock = null;
$ENTITY_ID = 0;
if (isset($_REQUEST['ENTITY_ID']))
    $ENTITY_ID = (int) $_REQUEST['ENTITY_ID'];
if ($ENTITY_ID > 0) {
    $hlblock = HL\HighloadBlockTable::getById($ENTITY_ID)->fetch();

    if (!empty($hlblock)) {
        //localization
        $lng = HL\HighloadBlockLangTable::getList(array(
                    'filter' => array('ID' => $hlblock['ID'], '=LID' => LANG))
                )->fetch();
        if ($lng) {
            $hblockName = $lng['NAME'];
        } else {
            $hblockName = $hlblock['NAME'];
        }
        //check rights
        if ($USER->isAdmin()) {
            $canEdit = $canDelete = true;
        } else {
            $operations = HL\HighloadBlockRightsTable::getOperationsName($ENTITY_ID);
            if (empty($operations)) {
                $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
            } else {
                $canEdit = in_array('hl_element_write', $operations);
                $canDelete = in_array('hl_element_delete', $operations);
            }
        }
    }
}

$APPLICATION->SetTitle(Loc::getMessage('BX_MODERNDIRECTORY_ROWS_LIST_PAGE_TITLE', array('#NAME#' => $hblockName)));

$entity = HL\HighloadBlockTable::compileEntity($hlblock);

/** @var HL\DataManager $entity_data_class */
$entity_data_class = $entity->getDataClass();
$entity_table_name = $hlblock['TABLE_NAME'];

$sTableID = 'tbl_' . $entity_table_name;
$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arHeaders = array(array(
        'id' => 'ID',
        'content' => 'ID',
        'sort' => 'ID',
        'default' => true
        ));

$ufEntityId = 'HLBLOCK_' . $hlblock['ID'];
$USER_FIELD_MANAGER->AdminListAddHeaders($ufEntityId, $arHeaders);

// show all columns by default
foreach ($arHeaders as &$arHeader) {
    $arHeader['default'] = true;
}
unset($arHeader);

$lAdmin->AddHeaders($arHeaders);

if (!in_array($by, $lAdmin->GetVisibleHeaderColumns(), true)) {
    $by = 'ID';
}

// add filter
$filter = null;

$filterFields = array('find_id');
$filterValues = array();
$filterTitles = array('ID');

$USER_FIELD_MANAGER->AdminListAddFilterFields($ufEntityId, $filterFields);

$filter = $lAdmin->InitFilter($filterFields);

if (!empty($find_id)) {
    $filterValues['ID'] = $find_id;
}

$USER_FIELD_MANAGER->AdminListAddFilter($ufEntityId, $filterValues);
$USER_FIELD_MANAGER->AddFindFields($ufEntityId, $filterTitles);

$filter = new CAdminFilter(
        $sTableID . "_filter_id",
        $filterTitles
);

// select data
/** @var string $order */
$order = strtoupper($order);

$usePageNavigation = true;

$navyParams = CDBResult::GetNavParams(CAdminResult::GetNavSize(
                        $sTableID,
                        array('nPageSize' => 20, 'sNavID' => $APPLICATION->GetCurPage() . '?ENTITY_ID=' . $ENTITY_ID)
        ));
if ($navyParams['SHOW_ALL']) {
    $usePageNavigation = false;
} else {
    $navyParams['PAGEN'] = (int) $navyParams['PAGEN'];
    $navyParams['SIZEN'] = (int) $navyParams['SIZEN'];
}

$selectFields = $lAdmin->GetVisibleHeaderColumns();
if (!in_array('ID', $selectFields))
    $selectFields[] = 'ID';
$getListParams = array(
    'select' => $selectFields,
    'filter' => $filterValues,
    'order' => array($by => $order)
);
unset($filterValues, $selectFields);
if ($usePageNavigation) {
    $getListParams['limit'] = $navyParams['SIZEN'];
    $getListParams['offset'] = $navyParams['SIZEN'] * ($navyParams['PAGEN'] - 1);
}

if ($usePageNavigation) {
    $countQuery = new Query($entity_data_class::getEntity());
    $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
    $countQuery->setFilter($getListParams['filter']);
    $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
    unset($countQuery);
    $totalCount = (int) $totalCount['CNT'];
    if ($totalCount > 0) {
        $totalPages = ceil($totalCount / $navyParams['SIZEN']);
        if ($navyParams['PAGEN'] > $totalPages)
            $navyParams['PAGEN'] = $totalPages;
        $getListParams['limit'] = $navyParams['SIZEN'];
        $getListParams['offset'] = $navyParams['SIZEN'] * ($navyParams['PAGEN'] - 1);
    } else {
        $navyParams['PAGEN'] = 1;
        $getListParams['limit'] = $navyParams['SIZEN'];
        $getListParams['offset'] = 0;
    }
}
$rsData = new CAdminResult($entity_data_class::getList($getListParams), $sTableID);
if ($usePageNavigation) {
    $rsData->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
    $rsData->NavRecordCount = $totalCount;
    $rsData->NavPageCount = $totalPages;
    $rsData->NavPageNomer = $navyParams['PAGEN'];
} else {
    $rsData->NavStart();
}

// build list
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));
$rows = [];
while ($arRes = $rsData->NavNext(true, "f_")) {
    
    $arRes_display = [];
    $ready_data = $USER_FIELD_MANAGER->getUserFieldsWithReadyData($ufEntityId, $arRes, LANGUAGE_ID);
    foreach ($ready_data as $code => $val) {
        
        if (is_array($val['VALUE'])) {
            $arRes_display[$code] = implode(", ", array_filter($val['VALUE'], function ($v) {return !empty(trim($v));}));
        } elseif ($val['USER_TYPE_ID'] === 'boolean') {
            $arRes_display[$code] = $val['VALUE'] > 0 ? Loc::getMessage('BX_MODERNDIRECTORY_ROW_YES_VALUE') : Loc::getMessage('BX_MODERNDIRECTORY_ROW_NO_VALUE');
        } else {
            $arRes_display[$code] = $val['VALUE'];
        }
    }
    
    $row = $lAdmin->AddRow($f_ID, $arRes_display);
    $rows[$arRes['ID']] = $arRes;
    $row->AddActions([
        [
            "DEFAULT" => "Y",
            "TEXT" => Loc::getMessage("BX_MODERNDIRECTORY_ROW_SELECT_TITLE"),
            "ACTION" => "javascript:moderndirectorySelectRow({$arRes['ID']})",
        ]
    ]);
}

$lAdmin->CheckListMode();
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
?>
<form name="find_form" method="GET" action="<? echo $APPLICATION->GetCurPage() ?>?ENTITY_ID=<?= $hlblock['ID'] ?>">
    <?
    $filter->Begin();
    ?>
    <tr>
        <td>ID</td>
        <td><input type="text" name="find_id" size="47" value="<? echo htmlspecialcharsbx($find_id) ?>"></td>
    </tr>
    <?
    $USER_FIELD_MANAGER->AdminListShowFilter($ufEntityId);
    $filter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage() . '?ENTITY_ID=' . $hlblock['ID'], "form" => "find_form"));
    $filter->End();
    ?>
</form>
<script>
    window.moderndirectorySelectRow = (row_id) => {

<?

// форматируем поля для отображения
foreach ($rows as $id => $row) {
    $ready_data = $USER_FIELD_MANAGER->getUserFieldsWithReadyData($ufEntityId, $row, LANGUAGE_ID);

    foreach ($ready_data as $fcode => $fdata) {
        
        $rows[$id][$fcode] = $fdata['VALUE'];
    }
}

?>

        let rows = <?= json_encode($rows); ?>;
        if (rows[row_id]) {
            window.opener.BX.Vue.event.$emit('<?= htmlspecialchars($_REQUEST['js_event']) ?>', {row: rows[row_id], rows_count: <?= intval($entity_data_class::getList(['select' => [new ExpressionField('CNT', 'COUNT(1)')]])->fetch()['CNT']); ?>});
            window.close();
        } else {
            alert('<?= Loc::getMessage('BX_MODERNDIRECTORY_ROW_WRONG_SELECT_TITLE') ?>');
        }
    };
</script>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
?>

