<?php
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @global CUser $USER */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
Bitrix\Main\Loader::includeModule('highloadblock');

$request = Bitrix\Main\Context::getCurrent()->getRequest();

$lang = $request->get('lang') ?: 'ru';

$directory = HL\HighloadBlockTable::getById(intval($request->get('directory_id')))->fetch();

$hl_lang = HL\HighloadBlockLangTable::getList(array(
            'filter' => array('ID' => $directory['ID'], '=LID' => $lang))
        )->fetch();

if ($hl_lang) {
    $directory['NAME_LANG'] = $hl_lang['NAME'];
} else {
    $directory['NAME_LANG'] = $directory['NAME'];
}

$can_edit = false;
//check rights
if ($USER->isAdmin()) {
    $can_edit = true;
} else {
    $operations = HL\HighloadBlockRightsTable::getOperationsName($directory['ID']);
    if (empty($operations)) {
        $APPLICATION->AuthForm(Loc::getMessage("BX_MODERNDIRECTORY_PERMISSION_DENIED"));
    } else {
        $can_edit = in_array('hl_element_write', $operations);
    }
}

if ($can_edit) {

    $row_id = intval($request->get('row_id'));

    $row_data = [];
    $action = "add";
    $directory_class = HL\HighloadBlockTable::compileEntity($directory)->getDataClass();
    if ($row_id) {
        $action = "update";
        $row_data = $directory_class::getList([
                    'filter' => ['ID' => $row_id]
                ])->fetch();
    }
    $errors = [];
    $form_request = false;
    if ($request->isPost() && $request->getPost('save') && check_bitrix_sessid()) {

        $form_request = true;

        $USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_' . $directory['ID'], $row_data);
        $save_data = $row_data;
        unset($save_data['ID']);
        if ($action === 'add') {
            // добавление записи
            $result = $directory_class::add($save_data);
            $row_id = $result->getId();
        } else {
            // обновление записи
            $result = $directory_class::update($row_id, $save_data);
        }

        if ($result->isSuccess()) {
            
            foreach ($save_data as $fcode => $fval) {
                if (is_array($fval)) {
                    $save_data[$fcode] = implode(', ', array_filter($fval, function ($v) {return !empty($v);}));
                } else {
                    $save_data[$fcode] = $fval;
                }
            }
            
            ?>
            <script>
                (() => {
                    let parent_window = window.opener;
                    let data = <?= json_encode($save_data) ?>;
                    data.ID = <?= $row_id ?>;
                    data.action = '<?= $action ?>';
                    parent_window.BX.Vue.event.$emit('<?= htmlspecialchars($request->get('js_event'))?>', {row: data, rows_count: <?= intval($directory_class::getList(['select' => [new ExpressionField('CNT', 'COUNT(1)')]])->fetch()['CNT']);?>});
                    window.close();
                })();
            </script>
            <?
        } else {
            $errors = $result->getErrorMessages();
            CAdminMessage::ShowMessage(join("\n", $errors));
        }
    }

    // form
    if ($action === 'add') {
        $title = Loc::getMessage('BX_MODERNDIRECTORY_FORM_ADD_TITLE');
    } else {
        $title = Loc::getMessage('BX_MODERNDIRECTORY_FORM_EDIT_TITLE', ["#ID#" => $row_id]);
    }
    $arr_tabs = array(
        array('DIV' => 'edit1', 'TAB' => $directory['NAME_LANG'], 'ICON' => 'ad_contract_edit', 'TITLE' => $title)
    );

    $tabControl = new CAdminForm('directory_row_edit_' . $directory['ID'], $arr_tabs);

    $tabControl->BeginPrologContent();

    echo CAdminCalendar::ShowScript();

    $tabControl->EndPrologContent();
    $tabControl->BeginEpilogContent();
    ?>
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="row_id" value="<?= $row_id ?>">
    <input type="hidden" name="js_event" value="<?= $request->get('js_event')?>">
    <input type="hidden" name="directory_id" value="<?= $directory['ID'] ?>">
    <input type="hidden" name="lang" value="<?= $lang ?>">
    <?
    $tabControl->EndEpilogContent();
    $tabControl->Begin(array(
        'FORM_ACTION' => $APPLICATION->GetCurPage() . '?directory_id=' . $directory['ID'] . '&row_id=' . $row_id . '&lang=' . $lang
    ));
    $tabControl->BeginNextFormTab();
    echo $tabControl->ShowUserFieldsWithReadyData('HLBLOCK_' . $directory['ID'], $row_data, $form_request, 'ID');
    $tabControl->Buttons(['disabled' => false, 'btnApply' => false]);
    $tabControl->Show();
} else {
    CAdminMessage::ShowMessage(Loc::getMessage('BX_MODERNDIRECTORY_PERMISSION_DENIED'));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
