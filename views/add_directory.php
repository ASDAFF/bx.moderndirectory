<?php
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @global CUser $USER */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Bitrix\Main\Loader::includeModule('highloadblock');
$request = Bitrix\Main\Context::getCurrent()->getRequest();
$allow_types = [
    "string" => Loc::getMessage("BX_MODERNDIRECTORY_STRING_TYPE_TITLE"),
    "number" => Loc::getMessage("BX_MODERNDIRECTORY_NUMBER_TYPE_TITLE"),
    "date" => Loc::getMessage("BX_MODERNDIRECTORY_DATE_TYPE_TITLE"),
    "datetime" => Loc::getMessage("BX_MODERNDIRECTORY_DATETIME_TYPE_TITLE"),
    "boolean" => Loc::getMessage("BX_MODERNDIRECTORY_BOOLEAN_TYPE_TITLE"),
    "file" => Loc::getMessage("BX_MODERNDIRECTORY_FILE_TYPE_TITLE"),
];
$directory_post_fields = [];
$directory_name = "";
$errors = [];
if (check_bitrix_sessid() && $request->isPost() && $request->getPost('save')) {

    $directory_name = trim($request->getPost('directory_name'));

    $directory_post_fields = $request->getPost('directory_fields');

    $directory_fields_to_create = [];

    foreach ($directory_post_fields as $k => $field) {
        if (trim(strip_tags($field['name'])) || trim(strip_tags($field['code']))) {
            // проверка полей
            $directory_fields_to_create[$k]['multiple'] = "N";
            $directory_fields_to_create[$k]['require'] = "N";
            foreach ($field as $fattr => $fvalue) {
                $fvalue = trim(strip_tags($fvalue));
                if ($fattr === 'name') {
                    if (!$fvalue) {
                        $errors['directory_fields'][$k]['name'] = "Необходимо указать название поля";
                    } else {
                        $directory_fields_to_create[$k]['name'] = $fvalue;
                    }
                } elseif ($fattr === 'type') {
                    if (!isset($allow_types[$fvalue])) {
                        $errors['directory_fields'][$k]['type'] = "Указан недопустимый тип поля";
                    } else {
                        $directory_fields_to_create[$k]['type'] = $fvalue;
                    }
                } elseif ($fattr === 'code') {
                    if (!$fvalue) {
                        $errors['directory_fields'][$k]['code'] = "Необходимо указать код поля";
                    } else {
                        $directory_fields_to_create[$k]['code'] = "UF_" . strtoupper($fvalue);
                    }
                } elseif ($fattr === 'multiple') {
                    if ($fvalue > 0) {
                        $directory_fields_to_create[$k]['multiple'] = "Y";
                    }
                } elseif ($fattr === 'require') {
                    if ($fvalue > 0) {
                        $directory_fields_to_create[$k]['require'] = "Y";
                    }
                }
            }
        }
    }
    
    if (empty($directory_fields_to_create)) {
        $errors['empty_directory_fields'] = "Не указаны поля справочника";
    }

    if ((!isset($errors['directory_fields']) || empty($errors['directory_fields'])) &&
            (!isset($errors['empty_directory_fields']) || empty($errors['empty_directory_fields']))) {

        // создаем справочник
        $result = HL\HighloadBlockTable::add([
                    'NAME' => ucfirst(strtolower($directory_name)),
                    'TABLE_NAME' => strtolower($directory_name)
        ]);
        if ($result->isSuccess()) {

            $id = $result->getId();
            $directory_fields_data = [];
            foreach ($directory_fields_to_create as $field) {
                $lang = [LANGUAGE_ID => $field['name']];
                $directory_fields_data[] = [
                    "ENTITY_ID" => 'HLBLOCK_' . $id,
                    "FIELD_NAME" => $field['code'],
                    "USER_TYPE_ID" => $field['type'],
                    "XML_ID" => "",
                    "SORT" => 100,
                    "MULTIPLE" => $field['multiple'],
                    'MANDATORY' => $field['require'],
                    'SHOW_FILTER' => 'Y',
                    'SHOW_IN_LIST' => 'Y',
                    'IS_SEARCHABLE' => 'N',
                    'EDIT_FORM_LABEL' => $lang,
                    'LIST_COLUMN_LABEL' => $lang,
                    'LIST_FILTER_LABEL' => $lang,
                    'ERROR_MESSAGE' => [
                        LANGUAGE_ID => 'An error in completing the field "' . $field['name'] . '"',
                    ]
                ];
            }
            $oUserTypeEntity = new CUserTypeEntity();

            foreach ($directory_fields_data as $arr_field) {

                if (!$oUserTypeEntity->Add($arr_field)) {
                    $errors['directory_fields_creation'][] = Loc::getMessage("BX_MODERNDIRECTORY_SAVE_FIELD_ERROR", [
                                "#FCODE#" => $arr_field["FIELD_NAME"],
                                "#ERROR#" => $oUserTypeEntity->LAST_ERROR
                    ]);
                }
            }
            if (!empty($errors['directory_fields_creation'])) {
                HL\HighloadBlockTable::delete($id);
            } else {
                ?><script>
                    (() => {
                        let parent_window = window.opener;
                        let event = new Event('<?= $js_event ?>');
                        event.directory = {
                            id: "<?= $directory_name ?>",
                            name: "<?= ucfirst(strtolower($directory_name)) ?>"
                        };

                        parent_window.document.dispatchEvent(event);
                        window.close();
                    })();

                </script><?
            }
        } else {
            $errors['directory_name'] = $result->getErrorMessages();
        }
    }
}
?>
<style>
    .directory-form__fields {
        margin-top: 30px;
    }
    .directory-form__btn-area {
        text-align: right;
    }
    .form-fields__row td{
        text-align: center;
    }
    .form-fields__required {
        color: red;
    }
    .form-fields tr:first-child td{
        padding-bottom: 30px;
    }
</style>
<?
if (!empty($errors['directory_name'])) {
    CAdminMessage::ShowMessage(implode("<br>", $errors['directory_name']));
}
if (!empty($errors['directory_fields_creation'])) {
    CAdminMessage::ShowMessage(implode("<br>", $errors['directory_fields_creation']));
}
if (!empty($errors['empty_directory_fields'])) {
    CAdminMessage::ShowMessage($errors['empty_directory_fields']);
}
?>
<form class="moderndirectory__form directory-form" action="<?= $APPLICATION->GetCurPage() ?>?js_event=<?= $request->get('js_event') ?>&lang=<?= LANGUAGE_ID ?>" method="post">
    <?= bitrix_sessid_post() ?>
    <table class="directory-form__fields form-fields" width="100%">
        <tr>
            <td width="40%" align="right"><b><?= Loc::getMessage("BX_MODERNDIRECTORY_TABLE_NAME_FIELD_TITLE") ?><span class="form-fields__required">*</span><br><small><?= Loc::getMessage("BX_MODERNDIRECTORY_TABLE_NAME_DESC_FIELD_TITLE") ?></small></b></td>
            <td width="60%" align="left"><input required pattern="[a-z]+" name="directory_name" type="text" value="<?= @$directory_name ?>"></td>
        </tr>
        <tr class="form-fields__row">
            <td colspan="2">
                <table width="100%" class="internal">
                    <thead>
                        <tr class="heading">
                            <td colspan="5"><?= Loc::getMessage('BX_MODERNDIRECTORY_TABLE_FIELDS_TITLE') ?><span class="form-fields__required">*</span></td>
                        </tr>
                        <tr class="heading">
                            <td><?= Loc::getMessage("BX_MODERNDIRECTORY_NAME_FIELD_TITLE") ?><span class="form-fields__required">*</span></td>
                            <td><?= Loc::getMessage("BX_MODERNDIRECTORY_TYPE_FIELD_TITLE") ?></td>
                            <td><?= Loc::getMessage("BX_MODERNDIRECTORY_MULTIPLE_FIELD_TITLE") ?></td>
                            <td><?= Loc::getMessage("BX_MODERNDIRECTORY_REQUIRED_FIELD_TITLE") ?></td>
                            <td><?= Loc::getMessage("BX_MODERNDIRECTORY_CODE_FIELD_TITLE") ?><span class="form-fields__required">*</span><br><small><?= Loc::getMessage("BX_MODERNDIRECTORY_CODE_DESC_FIELD_TITLE") ?></small></td>
                        </tr>
                    </thead>
                    <tbody>
                        <? for ($i = 0; $i <= 10; $i++): ?>
                            <tr>
                                <td>
                                    <? if (@$errors['directory_fields'][$i]['name']): ?>
                                        <span class="form-fields__required"><small><?= $errors['directory_fields'][$i]['name'] ?></small></span>
                                    <? endif ?>
                                    <input name="directory_fields[<?= $i ?>][name]" type="text" value="<?= @$directory_post_fields[$i]['name'] ?>">
                                </td>
                                <td>
                                    <? if (@$errors['directory_fields'][$i]['type']): ?>
                                        <span class="form-fields__required"><small><?= $errors['directory_fields'][$i]['type'] ?></small></span>
                                    <? endif ?>
                                    <select name="directory_fields[<?= $i ?>][type]">
                                        <? foreach ($allow_types as $type => $title): ?>
                                            <option <? if ($directory_post_fields[$i]['type'] === $type): ?>selected<? endif ?> value="<?= $type ?>"><?= $title ?></option>
                                        <? endforeach ?>
                                    </select>
                                </td>
                                <td><input <? if ($directory_post_fields[$i]['multiple'] > 0): ?>checked<? endif ?> type="checkbox" name="directory_fields[<?= $i ?>][multiple]" value="1"></td>
                                <td><input <? if ($directory_post_fields[$i]['require'] > 0): ?>checked<? endif ?> type="checkbox" name="directory_fields[<?= $i ?>][require]" value="1"></td>
                                <td>
                                    <? if (@$errors['directory_fields'][$i]['code']): ?>
                                        <span class="form-fields__required"><small><?= $errors['directory_fields'][$i]['code'] ?></small></span>
                                    <? endif ?>
                                    <input pattern="[A-Z_]+" name="directory_fields[<?= $i ?>][code]" type="text" value="<?= @$directory_post_fields[$i]['code'] ?>">
                                </td>
                            </tr>
                        <? endfor; ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <div class="directory-form__btn-area"><input type="submit" name="save" value="<?= Loc::getMessage("BX_MODERNDIRECTORY_SAVE_BTN_TITLE") ?>" class="adm-btn-save"></div>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
