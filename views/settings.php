<?php

use Bitrix\Main\Localization\Loc;

/**
 * @var BxModernDirectory self
 * @var array $arr_property
 * @var array $control_name
 * @var array $property_fields
 * @var CUserTypeManager $USER_FIELD_MANAGER
 */
$fields = [];
$html_id = randString(7);
if (!empty($arr_property["USER_TYPE_SETTINGS"]["HL_TABLE_NAME"])) {
    $fields = $USER_FIELD_MANAGER->GetUserFields("HLBLOCK_" . self::_directoryEntityId($arr_property["USER_TYPE_SETTINGS"]["HL_TABLE_NAME"]), [], LANGUAGE_ID);
}
?>

<tr>
    <td width="40%" style="text-align: right"></td>
    <!--<td width="60%" style="text-align: left"><a target="__blank" onclick="document.querySelector('.bx-core-adm-icon-close').dispatchEvent(new Event('click'))" href="/bitrix/admin/highloadblock_entity_edit.php?lang=<?= LANGUAGE_ID ?>"><?= Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_ADD_LINK_TITLE') ?></a></td>-->
    <td width="60%" style="text-align: left"><a onclick="BX.moderndirectory_settings_utils.add_directory()" href="javascript:void(0)"><?= Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_ADD_LINK_TITLE') ?></a></td>
</tr>
<?
$directories = self::_directoryList();

if (!empty($directories)):
    ?>
    <tr>
        <td ><?= Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_NAME_TITLE'); ?>:</td>
        <td >

            <select onchange="BX.moderndirectory_settings_utils.get_fields_list(this, '<?= $html_id ?>')" name="<?= $control_name["NAME"] ?>[HL_TABLE_NAME]">

                <option value=""><?= GetMessage("BX_MODERNDIRECTORY_CHOOSE") ?></option>
                <? foreach ($directories as $value => $title): ?>
                    <option <? if ($arr_property["USER_TYPE_SETTINGS"]["HL_TABLE_NAME"] == $value): ?>selected<? endif ?> value="<?= $value ?>"><?= $title ?></option>
                <? endforeach ?>
            </select>
        </td>
    </tr>
    <tr id="moderndirectory-setting-row-<?= $html_id ?>" class="moderndirectory__setting-row <? if (empty($fields)): ?>moderndirectory_hidden<? endif ?>">
        <td><?= Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_LIST_VALUE_TITLE'); ?>:</td>
        <td>

            <div class="moderndirectory__template-block template-block">
                <input type="text" value="<?= htmlspecialcharsbx($arr_property["USER_TYPE_SETTINGS"]["MASK_VALUE"]) ?>" name="<?= $control_name["NAME"] ?>[MASK_VALUE]" placeholder="<?= Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_LIST_VALUE_PLACEHOLDER_TITLE') ?>"> 
                <button class="template-block__btn" onclick="BX.moderndirectory_settings_utils.show_fields_block(this, '<?= $html_id ?>')" type="button" >...</button>
                <div class="moderndirectory_hidden template-block__fields fields">
                    <?
                    $json_fields = [];
                    foreach ($fields as $fcode => $fdata):
                        $title = $fdata['EDIT_FORM_LABEL'] ?: $fcode;
                        $json_fields[] = ['title' => $title, 'code' => $fcode];
                        ?>
                        <div class="fields__item">
                            <a href="javascript:void(0)" onclick="BX.moderndirectory_settings_utils.set_template(this, '<?= $fcode ?>')"><?= $title ?></a> - #<?= $fcode?>#
                        </div>
    <? endforeach ?>
                </div>
            </div>
        </td>
    </tr>
    <?
endif;
?>


<?$js_event = "directory-".randString(7)."-is-created"?>
<script>
    if (!BX.moderndirectory_settings_utils) {
        BX.namespace('moderndirectory_settings_utils');
        BX.moderndirectory_settings_utils = {
            __cache: {<? if (!empty($json_fields)): ?>'<?= $arr_property["USER_TYPE_SETTINGS"]["HL_TABLE_NAME"] ?>': <?= json_encode($json_fields) ?><? endif ?>},
            _create_field_item: (field) => {
                let div = document.createElement('div');
                let a = document.createElement('a');
                div.classList = 'fields__item';
                a.innerText = field.title;
                a.href = "javascript:void(0)";
                a.onclick = (e) => {
                    BX.moderndirectory_settings_utils.set_template(e.target, field.code);
                },
                div.appendChild(a);
                div.appendChild(document.createTextNode(` - #${field.code}#`));
                return div;
            },
            _fill_template_block: (tamplate_block_fields, table_name) => {
                tamplate_block_fields.innerText = '';
                BX.moderndirectory_settings_utils.__cache[table_name].forEach((field) => {
                    tamplate_block_fields.appendChild(BX.moderndirectory_settings_utils._create_field_item(field));
                });
            },
            add_directory: () => {
                window.jsUtils.OpenWindow(`/bitrix/admin/bxmoderndirectory_add_directory.php?js_event=<?= $js_event ?>&lang=<?= LANGUAGE_ID ?>`, 800, 850);
            },
            get_fields_list: (select, html_id) => {

                let table_name = select.value;
                let row = document.getElementById(`moderndirectory-setting-row-${html_id}`);
                let tamplate_block_fields = row.querySelector('.template-block__fields');
                row.querySelector('input[name="<?= $control_name["NAME"] ?>[MASK_VALUE]"]').value = "";
                if (!table_name) {
                    row.classList.add('moderndirectory_hidden');
                    tamplate_block_fields.classList.add('moderndirectory_hidden');
                } else {
                    row.classList.remove('moderndirectory_hidden');
                    if (BX.moderndirectory_settings_utils.__cache[table_name]) {
                        BX.moderndirectory_settings_utils._fill_template_block(tamplate_block_fields, table_name);
                    } else {
                        BX.ajax.get('/bitrix/admin/hloadblock_fields_ajax.php', {
                            sessid: BX.bitrix_sessid(),
                            table_name: table_name
                        }, (response) => {
                            response = JSON.parse(response);
                            if (!response.error) {
                                BX.moderndirectory_settings_utils.__cache[table_name] = response.result;
                                BX.moderndirectory_settings_utils._fill_template_block(tamplate_block_fields, table_name);
                            }

                        });
                    }
                }
                console.log(1);
            },
            show_fields_block: (btn) => {
                let tamplate_block_fields = btn.parentElement.querySelector('.template-block__fields');
                tamplate_block_fields.classList.toggle('moderndirectory_hidden');
            },
            set_template: (a, code) => {
                let parent = a.closest('.moderndirectory__template-block');
                let input = parent.querySelector('input[name="<?= $control_name["NAME"] ?>[MASK_VALUE]"]');
                input.value += `#${code}#`;
                input.focus();
                parent.querySelector('.template-block__fields').classList.add('moderndirectory_hidden');
            }
        };
        
        document.addEventListener('<?= $js_event?>', (e) => {
        
            let select = document.querySelector('select[name="<?= $control_name["NAME"] ?>[HL_TABLE_NAME]"]');
            let option = document.createElement('option');
            option.innerText = e.directory.name;
            option.value = e.directory.id;
            option.selected = true;
            select.appendChild(option);
            select.dispatchEvent(new Event('change'));
            console.log(0);
        });
        
    }
</script>
<style>
    .template-block__btn {
        cursor: pointer;
    }
    .moderndirectory__template-block {
        display: inline-block;
        position: relative;
    }
    .moderndirectory_hidden {
        display: none;
    }
    .template-block__fields {
        position: absolute;
        right: -183px;
        top: 0px;
        background: #fff;
        padding: 10px;
        width: 160px;
        max-height: 300px;
        margin-bottom: 20px;
        overflow-y: auto;
    }
    .fields__item {
        margin-bottom: 5px;
    }
</style>
