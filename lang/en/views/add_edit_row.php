<?php
$MESS['BX_MODERNDIRECTORY_PERMISSION_DENIED'] = "No rights to work with this directory";
$MESS['BX_MODERNDIRECTORY_FORM_ADD_TITLE'] = "Add new row";
$MESS['BX_MODERNDIRECTORY_FORM_EDIT_TITLE'] = "Edit row ##ID#";