<?php
$MESS['BX_MODERNDIRECTORY_MODULE_NAME'] = "Custom Property  \"Advanced Directory\"";
$MESS['BX_MODERNDIRECTORY_MODULE_DESC'] = "Custom Property  \"Advanced Directory\" for 1С-Битрикс: Управление сайтом";
$MESS['BX_MODERNDIRECTORY_PARTNER_NAME'] = "ИП Бреский Дмитрий Игоревич";
$MESS['BX_MODERNDIRECTORY_PARTNER_URI'] = "https://gitlab.com/dimabresky";
$MESS['BX_MODERNDIRECTORY_HL_NOT_INSTALLED'] = "An installed module is required highloadblock";
$MESS['BX_MODERNDIRECTORY_IB_NOT_INSTALLED'] = "An installed module is required iblock";

