<?php
$MESS['BX_MODERNDIRECTORY_MODULE_NAME'] = "Пользовательское свойство \"Продвинутый справочник\"";
$MESS['BX_MODERNDIRECTORY_MODULE_DESC'] = "Пользовательское свойство \"Продвинутый справочник\" для 1С-Битрикс: Управление сайтом";
$MESS['BX_MODERNDIRECTORY_PARTNER_NAME'] = "ИП Бреский Дмитрий Игоревич";
$MESS['BX_MODERNDIRECTORY_PARTNER_URI'] = "https://gitlab.com/dimabresky";
$MESS['BX_MODERNDIRECTORY_HL_NOT_INSTALLED'] = "Необходимо наличие установленного модуля highloadblock";
$MESS['BX_MODERNDIRECTORY_IB_NOT_INSTALLED'] = "Необходимо наличие установленного модуля инфоблоков";

