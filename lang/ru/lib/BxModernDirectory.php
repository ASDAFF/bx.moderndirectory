<?php
$MESS['BX_MODERNDIRECTORY_PROP_DESC'] = "Продвинутый справочник";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_NOT_FOUND'] = "Продвинутый справочник: справочник для свойства \"#CODE#\" не найден.";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_NAME_TITLE'] = "Справочник";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_ADD_LINK_TITLE'] = "Добавить новый справочник";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_LIST_VALUE_TITLE'] = "Укажите шаблон из значений полей для отображения элемента справочника";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_LIST_VALUE_PLACEHOLDER_TITLE'] = "";
$MESS['BX_MODERNDIRECTORY_DIRECTORY_NOT_SETTED'] = "Укажите справочник в настройках свойства";
$MESS['BX_MODERNDIRECTORY_ROW_TITLE'] = "Запись";
$MESS['BX_MODERNDIRECTORY_ROW_ADD_TITLE'] = "добавить запись";
$MESS['BX_MODERNDIRECTORY_ROW_CHOOSE_TITLE'] = "выбрать из списка";
$MESS['BX_MODERNDIRECTORY_ROW_EDIT_TITLE'] = "ред.-ть";
$MESS['BX_MODERNDIRECTORY_ROW_DELETE_TITLE'] = "удалить";
$MESS['BX_MODERNDIRECTORY_CHOOSE'] = "Выберите";
$MESS['BX_MODERNDIRECTORY_ROW_RETURN_TITLE'] = "вернуть";
$MESS['BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE'] = "(не установлено)";
