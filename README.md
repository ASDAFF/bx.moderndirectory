# bx.moderndirectory

## Свойство инфоблока "Продвинутый справочник" для 1C-Битрикс: Управление сайтом.

Альтернатива стандартному свойству "Справочник" для 1C-Битрикс: Управление сайтом.

Для установки свойства клонируйте репозиторий или скопируйте архив с модулем в папку /bitrix/modules или в /local/modules.
Перейдите в Маркетплейс->Установленные решения и установите модуль. Для работы со 
свойством просто создайте его в качестве свойства к элементу инфоблока и произведите
настройки свойства в форме настроек свойства к элементу инфоблока.

Данное свойство позволяет производить добавление/редактирование/привязку элементов справочника прямо в форме редактирования элемента инфоблока.
При этом возможность редактирования в списке/фильтрации/отображения свойства в публичном разделе сайта и смарт фильтре сохранены.

Требования: 1С-Битрикс: Управление сайтом >=18.5.180, php >= 7.0.0
