<?

use Bitrix\Highloadblock as HL;

/** @global CUser $USER */
/** @global CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);
define("PUBLIC_AJAX_MODE", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);
$request = Bitrix\Main\Context::getCurrent()->getRequest();
$result = [];
if ($USER->IsAuthorized() && check_bitrix_sessid() && $request->get('table_name')) {
    \Bitrix\Main\Loader::includeModule('highloadblock');

    $iterator = HL\HighloadBlockTable::getList([
                'select' => ['ID'],
                'filter' => ['=TABLE_NAME' => $request->get('table_name')]
    ]);
    $row = $iterator->fetch();
    
    if ($row['ID'] && $row['ID'] > 0) {
        
        $fields = $USER_FIELD_MANAGER->GetUserFields("HLBLOCK_" . $row['ID'], NULL, LANGUAGE_ID);
        
        foreach ($fields as $fcode => $fdata) {
            $title = $fcode;
            if ($fdata['EDIT_FORM_LABEL']) {
                $title = $fdata['EDIT_FORM_LABEL'];
            }
            
            $result[] = ['code' => $fcode, 'title' => $title];
        }
    }
}
echo json_encode(['error' => false, 'result' => $result]);
