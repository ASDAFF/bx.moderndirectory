<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class bx_moderndirectory extends CModule {

    public $MODULE_ID = "bx.moderndirectory";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = "N";

    function __construct() {
        $arModuleVersion = array();
        $path_ = str_replace("\\", "/", __FILE__);
        $path = substr($path_, 0, strlen($path_) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = Loc::getMessage("BX_MODERNDIRECTORY_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("BX_MODERNDIRECTORY_MODULE_DESC");
        $this->PARTNER_NAME = Loc::getMessage("BX_MODERNDIRECTORY_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("BX_MODERNDIRECTORY_PARTNER_URI");

        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
        set_time_limit(0);
    }

    public function DoInstall() {
        try {

            # проверка зависимостей модуля
            if (!ModuleManager::isModuleInstalled("iblock")) {
                $errors[] = Loc::getMessage("BX_MODERNDIRECTORY_IB_NOT_INSTALLED");
            }
            if (!ModuleManager::isModuleInstalled("highloadblock")) {
                $errors[] = Loc::getMessage("BX_MODERNDIRECTORY_HL_NOT_INSTALLED");
            }

            if (isset($errors) && !empty($errors)) {
                throw new Exception(implode("<br>", $errors));
            }

            # регистрируем модуль
            ModuleManager::registerModule($this->MODULE_ID);
            
            # добавление зависимостей модуля
            $this->addModuleDependencies();
            
            $this->copyFiles();
            
        } catch (Exception $ex) {

            $GLOBALS["APPLICATION"]->ThrowException($ex->getMessage());
            
            $this->DoUninstall();

            return false;
        }

        return true;
    }

    public function DoUninstall() {
        
        # удаляем зависимости модуля
        $this->deleteModuleDependencies();
        
        $this->deleteFiles();
        
        ModuleManager::unRegisterModule($this->MODULE_ID);

        return true;
    }
    
    public function copyFiles () {
        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bx.moderndirectory")) {
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bx.moderndirectory/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        } else {
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/bx.moderndirectory/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        }
    }
    
    public function deleteFiles () {
        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bx.moderndirectory")) {
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/bx.moderndirectory/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        } else {
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/bx.moderndirectory/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        }
    }
    
    public function addModuleDependencies () {
        
        RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "BxModernDirectory", "GetUserTypeDescription");
        
    }
    
    public function deleteModuleDependencies () {
        
        UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", $this->MODULE_ID, "BxModernDirectory", "GetUserTypeDescription");
        
    }
    
}
